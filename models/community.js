module.exports = (sequelize, DataTypes) => {
  const Community = sequelize.define('Community', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    pictureUrl: DataTypes.STRING,
    population: DataTypes.INTEGER,
    households: DataTypes.INTEGER,
    servedHouseholds: DataTypes.INTEGER,
    householdsWithoutWater: DataTypes.INTEGER,
    healthCentersCount: DataTypes.INTEGER,
    schoolsCount: DataTypes.INTEGER,
    wsp: DataTypes.FLOAT,
    wshl: DataTypes.FLOAT,
    wssi: DataTypes.FLOAT,
    wsl: DataTypes.FLOAT,
    shl: DataTypes.FLOAT,
    wsi: DataTypes.FLOAT,
    sep: DataTypes.FLOAT,
    wslAcc: DataTypes.FLOAT,
    wslCon: DataTypes.FLOAT,
    wslSea: DataTypes.FLOAT,
    wslQua: DataTypes.FLOAT,
    shlSsl: DataTypes.FLOAT,
    shlPer: DataTypes.FLOAT,
    shlWat: DataTypes.FLOAT,
    shlCom: DataTypes.FLOAT,
    wsiAut: DataTypes.FLOAT,
    wsiInf: DataTypes.FLOAT,
    wsiPro: DataTypes.FLOAT,
    wsiTre: DataTypes.FLOAT,
    sepOrg: DataTypes.FLOAT,
    sepOpm: DataTypes.FLOAT,
    sepEco: DataTypes.FLOAT,
    sepEnv: DataTypes.FLOAT,
    ecs: DataTypes.FLOAT,
    ecsEag: DataTypes.FLOAT,
    ecsCag: DataTypes.FLOAT,
    ecsShe: DataTypes.FLOAT,
    ecsShs: DataTypes.FLOAT,
    fis: DataTypes.FLOAT,
    fps: DataTypes.FLOAT,
    fhp: DataTypes.FLOAT,
    fus: DataTypes.FLOAT,
    ftb: DataTypes.FLOAT,
    fafl: DataTypes.FLOAT,
    wco: DataTypes.FLOAT,
    accf: DataTypes.FLOAT,
    householdsConSum: DataTypes.FLOAT,
    householdsSum: DataTypes.FLOAT,
    householdsQuaSum: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  Community.associate = function(models) {
    models.Community.belongsToMany(models.System, {
      through: models.SystemCommunity,
      foreignKey: 'community_id',
      otherKey: 'system_id',
      as: 'systems',
    });
    models.Community.belongsToMany(models.ServiceProvider, {
      through: models.ServiceProviderCommunity,
      foreignKey: 'community_id',
      otherKey: 'service_provider_id',
      as: 'serviceProviders',
    });
    models.Community.belongsToMany(models.TechnicalProvider, {
      through: models.TechnicalProviderCommunity,
      foreignKey: 'community_id',
      otherKey: 'technical_provider_id',
      as: 'technicalProviders',
    });
  };

  Community.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return Community;
};
