module.exports = (sequelize, DataTypes) => {
  const System = sequelize.define('System', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    sketchUrl: DataTypes.STRING,
    pictureUrl: DataTypes.STRING,
    buildingYear: DataTypes.INTEGER,
    servedHouseholds: DataTypes.INTEGER,
    supplyType: DataTypes.ARRAY(DataTypes.STRING),
    wsi: DataTypes.FLOAT,
    wsiAut: DataTypes.FLOAT,
    wsiInf: DataTypes.FLOAT,
    wsiPro: DataTypes.FLOAT,
    wsiTre: DataTypes.FLOAT,
    catf: DataTypes.FLOAT,
    traf: DataTypes.FLOAT,
    stof: DataTypes.FLOAT,
    disf: DataTypes.FLOAT,
    treatSust: DataTypes.FLOAT,
    treatPat: DataTypes.FLOAT,
    chlora: DataTypes.BOOLEAN,
    flow: DataTypes.JSON,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  System.associate = function(models) {
    models.System.belongsToMany(models.Community, {
      through: models.SystemCommunity,
      foreignKey: 'system_id',
      otherKey: 'community_id',
      as: 'communities',
    });
    models.System.belongsToMany(models.ServiceProvider, {
      through: models.ServiceProviderSystem,
      foreignKey: 'system_id',
      otherKey: 'service_provider_id',
      as: 'serviceProviders',
    });
  };

  System.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return System;
};
