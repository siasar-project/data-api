module.exports = (sequelize, DataTypes) => {
  const TechnicalProviderCommunity = sequelize.define('TechnicalProviderCommunity', {
    technicalProviderId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    communityId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    servedHouseholds: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    underscored: true,
  });
  return TechnicalProviderCommunity;
};
