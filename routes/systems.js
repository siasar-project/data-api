import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.System);

router.get('/stats', async (req, res) => {
  res.send({
    servedHouseholds: await models.System.sum('servedHouseholds', req.query),
  });
});

router.get('/supply-type-scores', async (req, res) => {
  const supplyType = models.sequelize.fn('unnest', models.sequelize.literal('supply_type'));
  const count = models.sequelize.fn('count', models.sequelize.literal('*'));

  const records = await models.System.findAll({
    attributes: [[supplyType, 'supplyType'], 'score', [count, 'count']],
    where: req.query.where,
    group: [supplyType, 'score'],
    order: [[count, 'DESC']],
    raw: true,
  }).reduce((o, e) => {
    if (!o[e.supplyType]) o[e.supplyType] = {};
    o[e.supplyType][e.score] = e.count;
    return o;
  }, {});

  if (req.query.percentage === 'true') {
    const totals = await models.System.findAll({
      attributes: [[supplyType, 'supplyType'], [count, 'count']],
      where: req.query.where,
      group: [supplyType],
      order: [supplyType],
      raw: true,
    }).reduce((o, e) => {
      o[e.supplyType] = e.count;
      return o;
    }, {});

    Object.keys(records).forEach((type) => {
      Object.keys(records[type]).forEach((score) => {
        records[type][score] = ((records[type][score] / totals[type]) * 100);
      });
    });
  }

  res.send(records);
});

router.get('/by-supply-type', async (req, res) => {
  const supplyType = models.sequelize.fn('unnest', models.sequelize.literal('supply_type'));
  const count = models.sequelize.fn('count', models.sequelize.literal('*'));

  const records = await models.System.findAll({
    attributes: [[supplyType, 'supplyType'], [count, 'count']],
    where: req.query.where,
    group: [supplyType],
    order: [[count, 'DESC']],
    raw: true,
  }).reduce((o, e) => {
    o[e.supplyType] = e.count;
    return o;
  }, {});

  res.send(records);
});

router.get('/:id', async (req, res) => {
  res.send(await models.System.findById(req.params.id, {
    include: [{
      model: models.Community,
      as: 'communities',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }, {
      model: models.ServiceProvider,
      as: 'serviceProviders',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }],
  }));
});

export default router;
