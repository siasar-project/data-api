import SphericalMercator from '@mapbox/sphericalmercator';

const mercator = new SphericalMercator();

export default function commonRoutes(router, model) {
  router.get('/', async (req, res) => {
    if (req.query.format === 'csv' || req.query.format === 'geojson') {
      req.query.raw = true;
    }

    let results = await model.findAll(req.query);

    if (req.query.format === 'csv') {
      res.setHeader('Content-disposition', `attachment; filename=${model.tableName}.csv`);
      return res.csv(results, true);
    }

    if (req.query.format === 'geojson') {
      results = {
        type: 'FeatureCollection',
        features: results.map((result) => {
          const geometry = { ...result.geom };
          delete result.geom;
          return {
            type: 'Feature',
            properties: result,
            geometry,
          };
        }),
      };
    }

    return res.send(results);
  });

  router.get('/count', async (req, res) => {
    const count = [[model.sequelize.fn('count', 'siasarId'), 'count']];
    req.query.attributes = req.query.group ? [...req.query.group, ...count] : count;

    res.send(await model.findAll(req.query));
  });

  router.get('/by-month', async (req, res) => {
    let records = await model.findAll({
      attributes: ['surveyYear', 'surveyMonth', [model.sequelize.fn('count', 'siasarId'), 'count']],
      where: req.query.where,
      group: ['surveyYear', 'surveyMonth'],
      order: ['surveyYear', 'surveyMonth'],
      raw: true,
    });

    records = records.map(e => ({
      date: new Date(`${e.surveyYear}-${e.surveyMonth}`),
      count: e.count,
    }));

    if (req.query.accumulated === 'true' && records.length) {
      const start = await model.count({
        where: {
          ...req.query.where,
          surveyDate: { $lt: records[0].date },
        },
      });

      records = records.reduce((a, r, i) => {
        let value = parseInt(r.count, 10) + (a[i - 1] ? a[i - 1].count : 0);
        if (i === 0) value += start;
        return [...a, { date: r.date, count: value }];
      }, []);
    }

    res.send(records);
  });

  router.get('/by-score', async (req, res) => {
    let records = await model.findAll({
      attributes: ['score', [model.sequelize.fn('count', 'siasarId'), 'count']],
      where: req.query.where,
      group: ['score'],
      order: ['score'],
      raw: true,
    });

    const a = records.filter(i => i.score === 'A');
    const b = records.filter(i => i.score === 'B');
    const c = records.filter(i => i.score === 'C');
    const d = records.filter(i => i.score === 'D');

    records = [
      { score: 'A', count: (a.length ? a[0].count : 0) },
      { score: 'B', count: (b.length ? b[0].count : 0) },
      { score: 'C', count: (c.length ? c[0].count : 0) },
      { score: 'D', count: (d.length ? d[0].count : 0) },
    ];

    if (req.query.percentage === 'true') {
      const total = await model.count({ where: req.query.where });
      records = records.map(value => ({
        score: value.score,
        count: (value.count / total) * 100,
      }));
    }

    res.send(records);
  });

  router.get('/bounds', async (req, res) => {
    const coords = (await model.findOne({
      attributes: [[model.sequelize.fn('ST_Extent', model.sequelize.literal('geom')), 'box']],
      where: req.query.where,
      raw: true,
    })).box.match(/[+-]?\d+(\.\d+)?/g);

    res.send([[parseFloat(coords[1]), parseFloat(coords[0])], [parseFloat(coords[3]), parseFloat(coords[2])]]);
  });

  router.get('/tiles/:z/:x/:y', async (req, res) => {
    const { z, x, y } = req.params;

    const extent = 4096;
    const buffer = 256;
    const srid = 3857;

    const bbox = mercator.bbox(x, y, z, false);

    const envelope = `ST_MakeEnvelope(${bbox[0]}, ${bbox[1]}, ${bbox[2]}, ${bbox[3]}, 4326)`;

    const query = `
      SELECT ST_AsMVT(q, 'point', ${extent}, 'geom')
      FROM (
        SELECT
            siasar_id, name, score, country,
            ST_AsMVTGeom(
              ST_Transform(geom, ${srid}),
              ST_Transform(${envelope}, ${srid}),
              ${extent},
              ${buffer},
              true) AS geom
        FROM ${model.tableName}
        WHERE geom && ST_Expand(${envelope}, 0.01)
          ${req.query.where && req.query.where.country ? `AND country = '${req.query.where.country}'` : ''}
      ) q
    `;

    const tile = (await model.sequelize.query(query, { type: model.sequelize.QueryTypes.SELECT }))[0];

    res.setHeader('Content-Type', 'application/x-protobuf');

    if (tile.st_asmvt.length === 0) res.status(204);

    res.send(tile.st_asmvt);
  });
}
