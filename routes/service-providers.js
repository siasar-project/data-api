import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.ServiceProvider);

router.get('/stats', async (req, res) => {
  res.send({
    preventiveMaintenance: await models.ServiceProvider.count({
      where: { preventiveMaintenance: true, ...req.query.where },
    }),
    recoverCosts: await models.ServiceProvider.count({ where: { profitable: true, ...req.query.where } }),
    notRecoverCosts: await models.ServiceProvider.count({ where: { profitable: false, ...req.query.where } }),
    notKnowCosts: await models.ServiceProvider.count({ where: { profitable: null, ...req.query.where } }),
  });
});

router.get('/women-scores', async (req, res) => {
  const topLimit = req.query.topLimit ? parseInt(req.query.topLimit, 10) : 3;

  const records = await models.ServiceProvider.findAll({
    attributes: ['score', 'womenCount', [models.sequelize.fn('count', 'siasarId'), 'count']],
    where: req.query.where,
    group: ['score', 'womenCount'],
    order: ['score', 'womenCount'],
    raw: true,
  }).reduce((o, e) => {
    if (!o[e.score]) o[e.score] = {};
    const value = parseInt(e.count, 10);
    if (e.womenCount >= topLimit) {
      if (!o[e.score][topLimit]) o[e.score][topLimit] = 0;
      o[e.score][topLimit] += value;
    } else {
      o[e.score][e.womenCount] = value;
    }
    return o;
  }, {});

  const results = [];

  for (let i = 0; i <= topLimit; i += 1) {
    results.push({
      womenCount: i,
      scores: Object.keys(records).reduce((o, e) => {
        o[e] = records[e][i] || 0;
        return o;
      }, {}),
    });
  }

  if (req.query.percentage === 'true') {
    const totals = await models.ServiceProvider.findAll({
      attributes: ['score', [models.sequelize.fn('count', 'siasarId'), 'count']],
      where: req.query.where,
      group: ['score'],
      order: ['score'],
      raw: true,
    }).reduce((o, e) => {
      o[e.score] = e.count;
      return o;
    }, {});

    results.forEach((result) => {
      Object.keys(records).forEach((score) => {
        result.scores[score] = ((result.scores[score] / totals[score]) * 100);
      });
    });
  }

  res.send(results);
});

router.get('/legal-status-scores', async (req, res) => {
  const records = await models.ServiceProvider.findAll({
    attributes: ['score', 'legalStatus', [models.sequelize.fn('count', 'siasarId'), 'count']],
    where: req.query.where,
    group: ['score', 'legalStatus'],
    order: ['score', 'legalStatus'],
    raw: true,
  }).reduce((o, e) => {
    o[e.legalStatus ? 'legalized' : 'notLegalized'][e.score] = e.count;
    return o;
  }, { legalized: {}, notLegalized: {} });

  if (req.query.percentage === 'true') {
    const totals = await models.ServiceProvider.findAll({
      attributes: ['score', [models.sequelize.fn('count', 'siasarId'), 'count']],
      where: req.query.where,
      group: ['score'],
      order: ['score'],
      raw: true,
    }).reduce((o, e) => {
      o[e.score] = e.count;
      return o;
    }, {});

    Object.keys(totals).forEach((score) => {
      records.legalized[score] = ((records.legalized[score] / totals[score]) * 100);
      records.notLegalized[score] = ((records.notLegalized[score] / totals[score]) * 100);
    });
  }

  res.send(records);
});

router.get('/:id', async (req, res) => {
  res.send(await models.ServiceProvider.findById(req.params.id, {
    include: [{
      model: models.Community,
      as: 'communities',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }, {
      model: models.System,
      as: 'systems',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }],
  }));
});

export default router;
