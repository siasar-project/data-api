import express from 'express';

import { description, version } from '../package.json';

import models from '../models';

const router = express.Router();

router.get('/', async (req, res) => {
  res.send({
    description,
    version,
  });
});

router.get('/stats', async (req, res) => {
  res.send({
    communities: await models.Community.count(req.query),
    systems: await models.System.count(req.query),
    serviceProviders: await models.ServiceProvider.count(req.query),
    technicalProviders: await models.TechnicalProvider.count(req.query),
    population: await models.Community.sum('population', req.query),
    households: await models.Community.sum('households', req.query),
    householdsWithoutWater: await models.Community.sum('householdsWithoutWater', req.query),
    servedHouseholds: await models.Community.sum('servedHouseholds', req.query),
  });
});

router.get('/countries', async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.sendFile('countries.geojson', { root: `${__dirname}/../data/` });
});

router.get('/favicon.ico', (req, res) => res.sendStatus(204));

export default router;
