import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.Community);

router.get('/stats', async (req, res) => {
  res.send({
    population: await models.Community.sum('population', req.query),
    households: await models.Community.sum('households', req.query),
    schools: await models.Community.sum('schoolsCount', req.query),
    healthCenters: await models.Community.sum('healthCentersCount', req.query),
  });
});

router.get('/water-coverage', async (req, res) => {
  const households = (await models.Community.sum('households', { where: req.query.where })) ?
    (await models.Community.sum('households', { where: req.query.where })) : null;

  res.send(await models.Community.findOne({
    attributes: [[models.sequelize.fn('sum', models.sequelize.literal(`wco * households / ${households}`)), 'wco']],
    where: { wco: { [models.sequelize.Op.ne]: 'null' }, ...req.query.where },
  }));
});

router.get('/sdgs', async (req, res) => {
  await models.Community.findAll({
    attributes: ['country'],
    group: ['country'],
    raw: true,
    where: {
      country: { [models.sequelize.Op.notIn]: ['PE', 'PY', 'CR', 'BR'] },
      ...req.query.where,
    },
    order: ['country'],
  }).then(async(countries) => {
    await Promise.all(countries.map(async (country, key) => {
      countries[key].households = await models.Community.findOne({
        attributes: [[models.sequelize.literal('SUM(households)'), 'households']],
        where: {
          country: countries[key].country,
          wslAcc: { [models.sequelize.Op.ne]: 'null' },
          wslCon: { [models.sequelize.Op.ne]: 'null' },
          wslQua: { [models.sequelize.Op.ne]: 'null' },
          ...req.query.where,
        },
        raw: true,
      }).then(i => i.households);

      countries[key].unimproved = await models.Community.findOne({
        attributes: [[models.sequelize.literal('SUM(households * (1-wco))'), 'unimproved']],
        where: {
          wco: { [models.sequelize.Op.ne]: 'Nan' },
          country: countries[key].country,
          wslAcc: { [models.sequelize.Op.ne]: 'null' },
          wslCon: { [models.sequelize.Op.ne]: 'null' },
          wslQua: { [models.sequelize.Op.ne]: 'null' },
          ...req.query.where,
        },
        raw: true,
      }).then(i => Math.round(i.unimproved));

      countries[key].limited = await models.Community.findOne({
        attributes: [[models.sequelize.literal('SUM(households * wco)'), 'limited']],
        raw: true,
        where: {
          country: countries[key].country,
          ...req.query.where,
          wslAcc: { [models.sequelize.Op.ne]: 'null' },
          wslCon: { [models.sequelize.Op.ne]: 'null' },
          wslQua: { [models.sequelize.Op.ne]: 'null' },
          accf: { [models.sequelize.Op.lt]: 0.9 },
          wco: { [models.sequelize.Op.gt]: 0 },
        },
      }).then(i => Math.round(i.limited));

      countries[key].managedSafely = await models.Community.findOne({
        attributes: [[models.sequelize.literal('SUM(households * wco)'), 'managedSafely']],
        raw: true,
        where: {
          country: countries[key].country,
          wco: { [models.sequelize.Op.gt]: 0 },
          wslAcc: { [models.sequelize.Op.ne]: 'null' },
          wslCon: { [models.sequelize.Op.ne]: 'null' },
          wslQua: { [models.sequelize.Op.ne]: 'null' },
          accf: { [models.sequelize.Op.gte]: 0.9 },
          ...req.query.where,
          managedSafely: models.sequelize.where(models.sequelize.literal('' +
          'households_con_sum/households_sum >= 0.5 AND households_qua_sum/households_sum = 1 --')),
        },
      }).then(i => Math.round(i.managedSafely));

      countries[key].count = await models.Community.count({
        attributes: [[models.sequelize.literal('count(siasar_Id)'), 'count']],
        raw: true,
        where: {
          country: countries[key].country,
          wslAcc: { [models.sequelize.Op.ne]: 'null' },
          wslCon: { [models.sequelize.Op.ne]: 'null' },
          wslQua: { [models.sequelize.Op.ne]: 'null' },
          ...req.query.where,
        },
      }).then(i => Math.round(i));

      countries[key].basic = countries[key].households - countries[key].unimproved -
        countries[key].limited - countries[key].managedSafely;
    }));

    if (req.query.percentage === 'true') {
      countries = countries.map(value => ({
        country: value.country,
        wco: Math.round(value.wco * 100),
        households: value.households,
        unimproved: parseFloat(((value.unimproved / value.households) * 100).toFixed(2)),
        limited: parseFloat(((value.limited / value.households) * 100).toFixed(2)),
        managedSafely: parseFloat(((value.managedSafely / value.households) * 100).toFixed(2)),
        basic: parseFloat(((value.basic / value.households) * 100).toFixed(2)),
      }));
    }
    res.send(countries);
  });
});

router.get('/:id', async (req, res) => {
  res.send(await models.Community.findById(req.params.id, {
    include: [{
      model: models.System,
      as: 'systems',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }, {
      model: models.ServiceProvider,
      as: 'serviceProviders',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }, {
      model: models.TechnicalProvider,
      as: 'technicalProviders',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }],
  }));
});

export default router;
