import express from 'express';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import 'express-async-errors';
import cors from 'cors';
import 'csv-express';

import index from './routes/index';
import communities from './routes/communities';
import systems from './routes/systems';
import serviceProviders from './routes/service-providers';
import technicalProviders from './routes/technical-providers';

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', index);
app.use('/communities', communities);
app.use('/systems', systems);
app.use('/service-providers', serviceProviders);
app.use('/technical-providers', technicalProviders);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  err = err.original || err;

  console.error(err);

  // response with error
  res.status(err.status || 500);
  res.send({
    code: err.code,
    message: err.message,
    type: err.routine,
    severity: err.severity,
  });
});

export default app;
